
TESTWALLET := $(shell pwd)/wallet


build:
	docker.io build -t ferranp/bitcoind .

run:
	docker.io run -d -v ${TESTWALLET}:/.bitcoin ferranp/bitcoind

runtest:
	docker.io run -i -t -v ${TESTWALLET}:/.bitcoin ferranp/bitcoind
